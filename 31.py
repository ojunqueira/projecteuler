"""
In England the currency is made up of pound, £, and pence, p, and there are eight coins in general circulation:

1p, 2p, 5p, 10p, 20p, 50p, £1 (100p) and £2 (200p).
It is possible to make £2 in the following way:

1×£1 + 1×50p + 2×20p + 1×5p + 1×2p + 3×1p
How many different ways can £2 be made using any number of coins?
"""

if __name__ == "__main__":
  t     = [1, 2, 5, 10, 20, 50, 100, 200]
  size  = len(t)
  num   = 200
  
  table     = [0 for k in range(num + 1)]
  table[0]  = 1
  for i in range(0, size):
    for j in range(t[i], num + 1):
      table[j] += table[j - t[i]]
  print("RESULT = ", table[num])
  

