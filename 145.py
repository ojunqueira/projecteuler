"""
Some positive integers n have the property that the sum [ n + reverse(n) ] consists entirely of odd (decimal) digits. For instance, 36 + 63 = 99 and 409 + 904 = 1313. We will call such numbers reversible; so 36, 63, 409, and 904 are reversible. Leading zeroes are not allowed in either n or reverse(n).

There are 120 reversible numbers below one-thousand.

How many reversible numbers are there below one-billion (109)?
"""

class List ():
  
  def __init__ (self):
    self.list = {}
  
  def add (self, value):
    if self.exists(value):
      return
    self.list[value] = True
    
  def count (self):
    return len(self.list)
  
  def exists (self, value):
    return self.list.get(value) == True

def get_reverse_number (n):
  assert type(n) == int
  reverse = str(n)
  ret     = ''
  for digit in reverse:
    ret = digit + ret
  return ret

def has_even_digits (n):
  assert type(n) == int
  for digit in str(n):
    if int(digit) % 2 == 0:
      return True
  return False
  

if __name__ == "__main__":
  
  listnumbers = List()
  for i in range(0, pow(10, 9)):
  # for i in range(0, pow(10, 3)):
    if i % 100 == 0:
      print("i=", i)
    if i % 10 == 0:
      continue
    # reversestr = str(i)[::-1]
    # reversenum = int(reversestr)
    reversenum = int(str(i)[::-1])
    # if reversestr[0] == '0':
    #   continue
    # mysum = i + reversenum
    if has_even_digits(i + reversenum):
      continue
    listnumbers.add(i)
    listnumbers.add(reversenum)
  ret = listnumbers.count()
  print("RESULT = ", ret)
  
  
